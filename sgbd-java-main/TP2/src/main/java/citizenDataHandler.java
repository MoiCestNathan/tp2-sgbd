import java.sql.Connection;
import java.sql.PreparedStatement;

public class citizenDataHandler {

    /**
     * Insère un citoyen dans la base de données.
     * @param nss       Numéro de sécurité sociale du citoyen.
     * @param nom       Nom du citoyen.
     * @param prenom    Prénom du citoyen.
     * @param sex       Sexe du citoyen.
     * @param conjoint  NSS du conjoint du citoyen, si applicable.
     * @param foyer     Code du foyer du citoyen, si applicable.
     */
    public static void insertCitizenData(String nss, String nom, String prenom, String sex, String conjoint, String foyer) {
        try {
            // Établir une connexion à la base de données.
            Connection connection = databaseConnection.getConnection();

            // Préparation de la requête SQL d'insertion.
            String insertQuery = "INSERT INTO Citoyen (NSS, Nom, Prenom, Sex, Conjoint, Foyer) VALUES (?, ?, ?, ?, ?, ?);";
            PreparedStatement preparedStatement = connection.prepareStatement(insertQuery);

            // Remplir les paramètres de la requête avec les données du citoyen.
            preparedStatement.setString(1, nss);
            preparedStatement.setString(2, nom);
            preparedStatement.setString(3, prenom);
            preparedStatement.setString(4, sex);
            preparedStatement.setString(5, conjoint);
            preparedStatement.setString(6, foyer);

            // Exécution de la requête d'insertion.
            int rowsAffected = preparedStatement.executeUpdate();

            // Vérification si l'insertion a été réussie.
            if (rowsAffected > 0) {
                System.out.println("Citoyen inséré avec succès.");
            } else {
                System.out.println("Aucun citoyen n'a été inséré.");
            }
        } catch (Exception e) {
            // Gestion des exceptions et affichage d'un message d'erreur.
            System.out.println("Une erreur est survenue lors de l'insertion du citoyen.");
            e.printStackTrace();
        }
    }
}
