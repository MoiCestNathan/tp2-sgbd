import java.util.Scanner;

public class main {
    public static void main(String[] args) {
        //databaseSetup.createTables();
        //dataInserter.insertData();

        Scanner scanner = new Scanner(System.in);

        // Demande des données de l'utilisateur
        System.out.print("Entrez le NSS : ");
        String nss = scanner.nextLine();
        System.out.print("Entrez le nom : ");
        String nom = scanner.nextLine();
        System.out.print("Entrez le prénom : ");
        String prenom = scanner.nextLine();
        System.out.print("Entrez le sexe (M/F) : ");
        String sex = scanner.nextLine();
        System.out.print("Entrez le NSS du conjoint (laisser vide si aucun) : ");
        String conjoint = scanner.nextLine();
        if (conjoint.isEmpty()) {
            conjoint = null;
        }
        System.out.print("Entrez le code du foyer (laisser vide si aucun) : ");
        String foyer = scanner.nextLine();
        if (foyer.isEmpty()) {
            foyer = null;
        }

        // Insérer les données dans la base de données
        citizenDataHandler.insertCitizenData(nss, nom, prenom, sex, conjoint, foyer);

        scanner.close();
    }

}
